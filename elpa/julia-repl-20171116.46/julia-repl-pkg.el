;;; -*- no-byte-compile: t -*-
(define-package "julia-repl" "20171116.46" "A minor mode for a Julia REPL" '((emacs "25")) :commit "f808a12e7ebe403f82036899c2dace640be73154" :keywords '("languages"))
