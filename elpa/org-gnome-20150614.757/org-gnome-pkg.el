;;; -*- no-byte-compile: t -*-
(define-package "org-gnome" "20150614.757" "Orgmode integration with the GNOME desktop" '((alert "1.2") (telepathy "0.1") (gnome-calendar "0.1")) :commit "122e14cf6f8104150a65246a9a7c10e1d7939862" :keywords '("org" "gnome"))
