;;; -*- no-byte-compile: t -*-
(define-package "tango-plus-theme" "20170214.908" "A color theme based on the tango palette" 'nil :url "https://github.com/tmalsburg/tango-plus-theme")
