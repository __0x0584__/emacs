;;; impatient-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "impatient-mode" "impatient-mode.el" (23149
;;;;;;  44065 30517 627000))
;;; Generated autoloads from impatient-mode.el

(autoload 'impatient-mode "impatient-mode" "\
Serves the buffer live over HTTP.

\(fn &optional ARG)" t nil)

;;;***

;;;### (autoloads nil nil ("impatient-mode-pkg.el") (23149 44064
;;;;;;  903467 771000))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; impatient-mode-autoloads.el ends here
