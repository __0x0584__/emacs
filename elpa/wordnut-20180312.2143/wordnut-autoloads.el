;;; wordnut-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "wordnut" "wordnut.el" (23214 28654 277502
;;;;;;  915000))
;;; Generated autoloads from wordnut.el

(autoload 'wordnut-search "wordnut" "\
Prompt for a word to search for, then do the lookup.

\(fn WORD)" t nil)

(autoload 'wordnut-lookup-current-word "wordnut" "\


\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("wordnut-history.el" "wordnut-pkg.el"
;;;;;;  "wordnut-u.el") (23214 28654 311517 489000))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; wordnut-autoloads.el ends here
