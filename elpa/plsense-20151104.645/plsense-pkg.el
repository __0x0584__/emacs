;;; -*- no-byte-compile: t -*-
(define-package "plsense" "20151104.645" "provide interface for PlSense that is a development tool for Perl." '((auto-complete "1.4.0") (log4e "0.2.0") (yaxception "0.2.0")) :commit "d50f9dccc98f42bdb42f1d1c8142246e03879218" :url "https://github.com/aki2o/emacs-plsense" :keywords '("perl" "completion"))
