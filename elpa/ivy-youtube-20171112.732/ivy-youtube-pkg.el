;;; -*- no-byte-compile: t -*-
(define-package "ivy-youtube" "20171112.732" "Query YouTube and play videos in your browser" '((request "0.2.0") (ivy "0.8.0") (cl-lib "0.5")) :commit "23e1089d4c4fc32db20df14ba10078aabf117e87" :url "https://github.com/squiter/ivy-youtube" :keywords '("youtube" "multimedia" "mpv" "vlc"))
