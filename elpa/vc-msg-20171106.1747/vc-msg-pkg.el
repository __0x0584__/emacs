(define-package "vc-msg" "20171106.1747" "Show commit information of current line"
  '((emacs "24.3")
    (popup "0.5.0"))
  :url "http://github.com/redguardtoo/vc-msg" :keywords
  '("git" "vc" "svn" "hg" "messenger"))
;; Local Variables:
;; no-byte-compile: t
;; End:
