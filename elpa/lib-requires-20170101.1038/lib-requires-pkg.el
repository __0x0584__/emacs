;;; -*- no-byte-compile: t -*-
(define-package "lib-requires" "20170101.1038" "Commands to list Emacs Lisp library dependencies." 'nil :url "http://www.emacswiki.org/lib-requires.el" :keywords '("libraries" "files"))
