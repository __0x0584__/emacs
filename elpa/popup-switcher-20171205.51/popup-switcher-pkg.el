;;; -*- no-byte-compile: t -*-
(define-package "popup-switcher" "20171205.51" "switch to other buffers and files via popup." '((cl-lib "0.3") (popup "0.5.3")) :commit "f5788a31918e37bb5c04139048c667bcec9f1b62" :url "https://github.com/kostafey/popup-switcher" :keywords '("popup" "switch" "buffers" "functions"))
