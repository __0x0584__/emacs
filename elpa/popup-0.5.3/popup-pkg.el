;;; -*- no-byte-compile: t -*-
(define-package "popup" "0.5.3" "Visual Popup User Interface" '((cl-lib "0.5")) :authors '(("Tomohiro Matsuyama" . "m2ym.pub@gmail.com")) :maintainer '("Tomohiro Matsuyama" . "m2ym.pub@gmail.com"))
